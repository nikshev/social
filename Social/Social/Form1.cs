﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Awesomium.Core;
using System.Threading;
using System.Data.SQLite;
using System.Linq;
using System.Windows;
using Awesomium.Core;
using System.Collections.Generic;
using Awesomium.Windows.Controls;


namespace Social
{
    public partial class Form1 : Form
    {
        private string message="";
        private BaseConnect baseConnect;
        private bool finishedLoading = false;

        public bool IsPopupView
        {
            get;
            set;
        }
        /// <summary>
        /// Form initialization (constructor)
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            this.baseConnect = new BaseConnect();
            this.FillTree();
        }

        /// <summary>
        /// Event  button1_Click.
        /// Get text from adress field (Text1) and send it to webControl
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                finishedLoading = false;
                Cursor.Current = Cursors.WaitCursor;
                if (textBox1.Text.Contains("http://")|| textBox1.Text.Contains("https://"))
                    webControl1.Source = new Uri(textBox1.Text);
                else
                    webControl1.Source = new Uri("http://"+textBox1.Text);
                while (!finishedLoading)
                {
                    Thread.Sleep(100);
                    WebCore.Update();
                }
                 Cursor.Current = Cursors.Default;
                 textBox1.Text = addressBox1.URL.ToString();
            }
            catch (Exception Ex)
            {
                message += "Exception in method button1_Click: " + Ex.Message + "\r\n";
            }
        }

        /// <summary>
        /// Event textBox1_KeyUp
        /// Method waiting for enter key
        /// </summary>
        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                this.button1_Click(null,null);
        }


        /// <summary>
        /// Event  Awesomium_Windows_Forms_WebControl_LoadingFrameComplete
        /// Method waiting for complete webControl load
        /// </summary>
        private void Awesomium_Windows_Forms_WebControl_LoadingFrameComplete(object sender, FrameEventArgs e)
        {
            if (e.IsMainFrame)
            {
                finishedLoading = true;
                textBox1.Text = addressBox1.URL.ToString();
            }
            Cursor.Current = Cursors.Default;
        }


        /// <summary>
        /// Fill the social tree
        /// </summary>
        private void FillTree()
        {
            List<int> socials_id;
            List<int> accounts;
            TreeNode node;
            int social_id;
            treeView1.Nodes.Clear();
            socials_id=this.baseConnect.GetSocial();
            for (int i = 0; i < socials_id.Count; i++)
            {
                social_id = socials_id.ElementAt(i);
                node = new TreeNode(this.baseConnect.GetSocialName(social_id));
                accounts = this.baseConnect.GetAccountsBySocial(social_id);
                for (int j = 0; j < accounts.Count; j++)
                    node.Nodes.Add(this.baseConnect.GetAccountUser(accounts.ElementAt(j)));
                treeView1.Nodes.Add(node);
            }
        }

        /// <summary>
        /// Add account event from context menu
        /// </summary>
        private void addAcountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string social_name = treeView1.SelectedNode.Text;
            AddAccountForm addAccountForm = new AddAccountForm(social_name);
            addAccountForm.ShowDialog();
            this.FillTree();
        }

        /// <summary>
        /// Node mouse click event
        /// </summary>
        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
               treeView1.SelectedNode = e.Node;
               if (e.Node.FullPath.Contains("\\"))
               {
                   treeContextMenuStrip.Items[0].Enabled = false;
                   treeContextMenuStrip.Items[1].Enabled = true;
               }
               else
               {
                   treeContextMenuStrip.Items[0].Enabled = true;
                   treeContextMenuStrip.Items[1].Enabled = false;
               }

               treeContextMenuStrip.Show(treeView1, e.Location);
            }
        }


        /// <summary>
        /// Login to social
        /// </summary>
        /// <param name="user">User name</param>
        /// <param name="pass">User password</param>
        private bool Login(string user, string pass, string social)
        {
            bool retVal = false;
            int social_id = this.baseConnect.GetSocialIDByName(social);
            string url = this.baseConnect.GetSocialUrl(social_id);
            string login_field = this.baseConnect.GetSocialLoginField(social_id);
            string pass_field = this.baseConnect.GetSocialPassField(social_id);
            string submit = this.baseConnect.GetSocialSubmit(social_id);
            try
            {
                finishedLoading = false;
                if (url.Contains("http://") || url.Contains("https://"))
                    webControl1.Source = new Uri(url);
                else
                    webControl1.Source = new Uri("http://" + url);
               
                while (!finishedLoading)
                {
                    Thread.Sleep(100);
                    WebCore.Update();
                }

                dynamic  document = (JSObject)webControl1.ExecuteJavascriptWithResult("document");
                
                while (true)
                {
                    if (document != null && document.ToString().Length > 0)
                        break;

                    document = (JSObject)webControl1.ExecuteJavascriptWithResult("document");
                }
                            
                if (document != null)
                {
                    using (document)
                    {
                        dynamic login_text;
                        dynamic pass_text;
                                                
                        login_text = document.getElementById(login_field);
                        pass_text = document.getElementById(pass_field);

                        if (login_text != null)
                        {
                            login_text.value=user;
                            if (pass_text != null)
                            {
                                pass_text.value=pass;

                                if (submit == "")
                                {
                                    dynamic elements;
                                
                                    ///elements = (JSObject)webControl1.ExecuteJavascriptWithResult("document.getElementsByTagName('input')");
                                    elements = (JSObject)webControl1.ExecuteJavascriptWithResult("document.getElementsByTagName('*')");

                                    if (elements == null)
                                        return retVal;

                                    int length = elements.length;

                                    if (length == 0)
                                        return retVal;

                                    using (elements)
                                    {
                                        string type = "";
                                        string value="";
                                        for (int i = 0; i < length; i++)
                                        {
                                            type = elements[i].getAttribute("type");
                                            value=elements[i].getAttribute("value");

                                            if (elements[i].getAttribute("type") == "submit")
                                            {
                                               elements[i].click();
                                               retVal = true;
                                               break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    dynamic submit_btn = document.getElementById(submit);
                                    submit_btn.click();
                                    retVal = true;
                                }
                            }
                            else
                                this.message += "Method: Login; Exception: Can't load pass field!\r\n";
                        }
                        else
                            this.message += "Method: Login; Exception: Can't load login field!\r\n";
                    }
                }
                else
                    this.message += "Method: Login; Exception: Can't load document!\r\n";
            }
            catch (Exception Ex)
            {
                message += "Exception in method Login: " + Ex.Message + "\r\n";
            }

            return retVal;
        }

        /// <summary>
        /// Try to login
        /// </summary>
        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int index = treeView1.SelectedNode.FullPath.IndexOf("\\");
            string user_name = treeView1.SelectedNode.Text;
            string social_name = treeView1.SelectedNode.FullPath.Substring(0, index);
            int account_id=this.baseConnect.GetAccountIDByUSer(user_name, social_name);
            string pass = this.baseConnect.GetAccountPass(account_id);
            while (!this.Login(user_name, pass, social_name))
                Thread.Sleep(100);
            Cursor.Current = Cursors.Default;
            textBox1.Text = addressBox1.URL.ToString();
        }

        /// <summary>
        /// Catch popup and  show it on main window
        /// </summary>
        private void Awesomium_Windows_Forms_WebControl_ShowCreatedWebView(object sender, ShowCreatedWebViewEventArgs e)
        {
            if (e.IsPopup || e.IsWindowOpen || e.IsPost)
            {
                IsPopupView = true;
                webControl1.Source = e.TargetURL;
            }
               
        }

        /// <summary>
        /// On double click login too
        /// </summary>
        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
                treeView1.SelectedNode = e.Node;
                if (e.Node.FullPath.Contains("\\"))
                {
                    this.loginToolStripMenuItem_Click(null, null);
                }
        }

        /// <summary>
        ///This event need for correct popup work
        /// </summary>
        private void Awesomium_Windows_Forms_WebControl_LoadingFrameFailed(object sender, Awesomium.Core.LoadingFrameFailedEventArgs e)
        {
            if (IsPopupView && e.IsMainFrame && e.ErrorCode == NetError.ABORTED)
            {
                webControl1.Source = e.Url;
            }
            Cursor.Current = Cursors.Default;
        }

        /// <summary>
        /// Go back
        /// </summary>
        private void backButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            finishedLoading = false;
            webControl1.GoBack();
            while (!finishedLoading)
            {
                Thread.Sleep(100);
                WebCore.Update();
            }
        }

        /// <summary>
        /// Go forward
        /// </summary>
        private void forwardButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            finishedLoading = false;
            webControl1.GoForward();
            while (!finishedLoading)
            {
                Thread.Sleep(100);
                WebCore.Update();
            }
        }

        /// <summary>
        /// Reload
        /// </summary>
        private void refreshButton_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            finishedLoading = false;
            webControl1.Reload(true);
            while (!finishedLoading)
            {
                Thread.Sleep(100);
                WebCore.Update();
            }
        }

    }
}
