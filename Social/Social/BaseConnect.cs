﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Social
{
    class BaseConnect
    {
        private string message = "";
        private SQLiteConnection connection;
        private SQLiteCommand command;
        private int ConnectionTimeout = 100;

        /**
         * Constructor 
         */
        public BaseConnect()
         {
             string connectionString = "Data Source=" +
                                      System.Environment.CurrentDirectory +
                                      "\\social.sqlite;Version=3;New=False;Compress=True;";
             connection = new SQLiteConnection(connectionString);
             command = new SQLiteCommand();
             command.Connection = connection;
         }

        /// <summary>
        /// Create new account and add account info to database
        /// </summary>
        /// <param name="user">User name</param>
        /// <param name="pass">Password  for social media</param>
        /// <param name="phone">User phone</param>
        /// <param name="email">User email</param>
        /// <param name="social_id">User email</param>
        public void AddAccount(string user, string pass, string phone, string email, int social_id)
        {
            string queryString = "INSERT INTO accounts (user_name,pass,phone,email,social_id)"+
                                  "VALUES ('" + user + "','" + pass + "','" + phone + "','" + email + "'," + social_id + ")";
            try
            {
                connection.Open();
                command.CommandText = queryString;
                command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: AddAccount; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }           
        }

        /// <summary>
        /// Get account id by user name
        /// </summary>
        /// <param name="user">User name</param>
        /// <param name="social">social name</param>
        public int GetAccountIDByUSer(string user,string social)
        {
            int retVal = -1;
            int social_id = this.GetSocialIDByName(social);
            string queryString = "SELECT id FROM accounts WHERE user_name='" + user 
                                 + "' and social_id="+social_id.ToString();
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = Int32.Parse(reader[0].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method:  GetAccountIDByUSer; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get account id by user email
        /// </summary>
        ///  <param name="email">User email</param>
        public int GetAccountIDByEmail(string email)
        {
            int retVal = -1;
            string queryString = "SELECT id FROM accounts WHERE email='" + email + "'";
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = Int32.Parse(reader[0].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccountIDByEmail; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get account id by user phone
        /// </summary>
        ///  <param name="phone">User phone</param>
        public int GetAccountIDByPhone(string phone)
        {
            int retVal = -1;
            string queryString = "SELECT id FROM accounts WHERE phone='" + phone+"'";
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal =Int32.Parse(reader[0].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccountIDByPhone; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get accounts id by 
        /// </summary>
        ///  <param name="social_id">Id of social media</param>
        public List<int> GetAccountsBySocial(int social_id)
        {
            List<int> retVal = new List<int>();
            string queryString = "SELECT id FROM accounts WHERE social_id=" + social_id.ToString();
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    while (reader.Read())
                        retVal.Add(Int32.Parse(reader[0].ToString()));
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccounts; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get accounts from all social media
        /// </summary>
        public List<int> GetAccounts()
        {
            List<int> retVal = new List<int>();
            string queryString = "SELECT id FROM accounts";
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    while (reader.Read())
                        retVal.Add(Int32.Parse(reader[0].ToString()));
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccounts; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }


        /// <summary>
        /// Get account user name by account id 
        /// </summary>
        ///  <param name="account_id">Account unique id</param>
        public string GetAccountUser(int account_id)
        {
            string retVal = "";
            string queryString = "SELECT user_name FROM accounts WHERE id=" + account_id.ToString();
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccountUser; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get account password by account id 
        /// </summary>
        ///  <param name="account_id">Account unique id</param>
        public string GetAccountPass(int account_id)
        {
            string retVal = "";
            string queryString = "SELECT pass FROM accounts WHERE id=" + account_id.ToString();
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccountPass; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get account phone by account id 
        /// </summary>
        ///  <param name="account_id">Account unique id</param>
        public string GetAccountPhone(int account_id)
        {
            string retVal = "";
            string queryString = "SELECT phone FROM accounts WHERE id=" + account_id.ToString();
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccountPhone; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get account email by account id 
        /// </summary>
        ///  <param name="account_id">Account unique id</param>
        public string GetAccountEmail(int account_id)
        {
            string retVal = "";
            string queryString = "SELECT email FROM accounts WHERE id=" + account_id.ToString();
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetAccountEmail; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }


        /// <summary>
        /// Get error message 
        /// </summary>
        public string GetMessage()
        {
            string retVal = this.message;
            this.message = "";
            return retVal;
        }

        /// <summary>
        /// Get social list
        /// </summary>
        public List<int> GetSocial()
        {
            List<int> retVal = new List<int>();

            string queryString = "SELECT id FROM Social";
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    while (reader.Read())
                       retVal.Add(Int32.Parse(reader[0].ToString()));
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetSocial; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }

            return retVal;
        }

        /// <summary>
        /// Get social name by id
        /// </summary>
        ///  <param name="social_id">Social unique id</param>
        public string GetSocialName(int social_id)
        {
            string retVal = "";
            string queryString = "SELECT social_name FROM Social WHERE id=" + social_id.ToString();
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal=reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetSocialName; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get social id by name
        /// </summary>
        ///  <param name="social_name">Social name</param>
        public int GetSocialIDByName(string social_name)
        {
            int retVal = -1;
            string queryString = "SELECT id FROM Social WHERE social_name='" + social_name+"'";
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal =Int32.Parse(reader[0].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method:  GetSocialIDByName; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }


        /// <summary>
        /// Get social url by social id
        /// </summary>
        ///  <param name="social_id">Social unique id</param>
        public string GetSocialUrl(int social_id)
        {
            string retVal = "";
            string queryString = "SELECT url FROM Social WHERE id=" + social_id;
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal =reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetSocialUrl; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }


        /// <summary>
        /// Get social login field by social id
        /// </summary>
        ///  <param name="social_id">Social unique id</param>
        public string GetSocialLoginField(int social_id)
        {
            string retVal = "";
            string queryString = "SELECT login_id FROM Social WHERE id=" + social_id;
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method:  GetSocialLoginField; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }


        /// <summary>
        /// Get social password field by social id
        /// </summary>
        ///  <param name="social_id">Social unique id</param>
        public string GetSocialPassField(int social_id)
        {
            string retVal = "";
            string queryString = "SELECT pass_id FROM Social WHERE id=" + social_id;
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetSocialPassField; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

        /// <summary>
        /// Get social password field by social id
        /// </summary>
        ///  <param name="social_id">Social unique id</param>
        public string GetSocialSubmit(int social_id)
        {
            string retVal = "";
            string queryString = "SELECT button_id FROM social WHERE id=" + social_id;
            try
            {
                connection.Open();
                command.CommandText = queryString;
                using (SQLiteDataReader reader = command.ExecuteReader())
                {
                    // Call Read before accessing data.
                    if (reader.Read())
                        retVal = reader[0].ToString();
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                this.message += "Method: GetSocialSubmit; Source:" + ex.Source + "; Exception:" + ex.Message + "\r\n";
                connection.Close();
            }
            return retVal;
        }

    }
}
