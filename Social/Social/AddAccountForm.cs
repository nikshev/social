﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Social
{
    public partial class AddAccountForm : Form
    {
        private BaseConnect baseConnect;
        private int social_id;

        /// <summary>
        /// Form initialization
        /// </summary>
        /// <param name="social_name">name of social media</param>
        public AddAccountForm(string social_name)
        {
            InitializeComponent();
            this.baseConnect = new BaseConnect();
            this.Text = "Add acount to " + social_name;
            this.social_id = this.baseConnect.GetSocialIDByName(social_name);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (this.userTextBox.Text != "")
            {
                if (this.passwordTextBox.Text != "")
                {
                    if (this.emailTextBox.Text != "")
                    {
                        if (this.phoneTextBox.Text != "")
                        {
                            this.baseConnect.AddAccount(userTextBox.Text, passwordTextBox.Text, phoneTextBox.Text, emailTextBox.Text, this.social_id);
                            this.Close();
                        }
                        else
                            MessageBox.Show("Error: Phone is undefined!");
                    }
                    else
                        MessageBox.Show("Error: Email is undefined!");
                }
                else
                    MessageBox.Show("Error: Password is undefined!");
            }
            else
                MessageBox.Show("Error: User name is undefined!");
        }
    }
}
